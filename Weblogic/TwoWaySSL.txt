Commands

Preparing Clientstore - Generating private-public for client

keytool -genkeypair -keystore client.jks -storepass welcome1 -alias 100bytesClient -keypass welcome1 -keyalg RSA
keytool -certreq -keystore client.jks -storepass welcome1 -alias 100bytesClient -keypass welcome1 -file 100bytesClient.csr -v
keytool -gencert -alias rootca -keypass welcome1 -keystore ..\castore\castore.jks -storepass welcome1 -ext BC=2 -rfc -infile 100bytesClient.csr -outfile 100bytesClient.cer

Installing Certificate reply to client store

keytool -importcert -alias rootca -keypass welcome1 -keystore client.jks -storepass welcome1 -file ..\castore\rootca.cer
keytool -importcert -alias 100bytesClient -keypass welcome1 -keystore client.jks -storepass welcome1 -file 100bytesClient.cer

Exporting client side key and certificate to PKCS12 format

keytool -importkeystore -srckeystore client.jks -srcstorepass welcome1 -destkeystore client_pkcs12.p12 -deststorepass welcome1 -deststoretype PKCS12 -alias 100bytesClient -v
