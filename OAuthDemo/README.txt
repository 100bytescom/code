OAUTH2.0 and OPENID demo
========================

Introduction:
-------------
	This project runs with the help of Grizzly server.
	MainGate is the starting point.
	

How to test:
------------
	Add the libraries for Grizzly, JWT and JSON.
	Import the self-signed certificate<PROJECT_LOCATION>/src/com/one00bytes/resource/keystore/castore/ca.cer
	Run the MainGate class as normal java file, which will start the server.
	Access the http://<HOSTNAME>:5001/public/index.html


Run on different server:
-----------------------
	If you want to run this project with different server, 
	just import "com.one00bytes.oauth.flows" to your project.
