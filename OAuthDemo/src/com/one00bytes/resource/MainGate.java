package com.one00bytes.resource;

import java.io.File;
import java.net.InetAddress;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.NetworkListener;
import org.glassfish.grizzly.http.server.ServerConfiguration;
import org.glassfish.grizzly.http.server.StaticHttpHandler;
import org.glassfish.grizzly.ssl.SSLContextConfigurator;
import org.glassfish.grizzly.ssl.SSLEngineConfigurator;

import com.one00bytes.oauth.flows.AuthzCodeFlow;
import com.one00bytes.oauth.flows.ClientCredentialFlow;
import com.one00bytes.oauth.flows.ClientSecretBasicAuthMode;
import com.one00bytes.oauth.flows.ClientSecretJwtAuthMode;
import com.one00bytes.oauth.flows.ClientSecretPostAuthMode;
import com.one00bytes.oauth.flows.ClientSecretPrivateKeyAuthMode;
import com.one00bytes.oauth.flows.ImplicitFlow;
import com.one00bytes.oauth.flows.ImplicitFlowRedirector;
import com.one00bytes.oauth.flows.ResourceOwnerFlow;
import com.one00bytes.oauth.flows.RefreshTokenFlow;
import com.one00bytes.oauth.flows.TokenIntrospectFlow;
import com.one00bytes.oauth.flows.TokenRevokeFlow;
import com.one00bytes.oauth.flows.AuthzCodeFlowRedirector;



/**
 * Main gate for the whole package. This project runs with the help of grizzly server
 * 
 * @author 100bytes.com
 * WEBSITE: http://100bytes.com
 */

public class MainGate {
	

	public static void main(String[] args) throws Exception {
		
		String serverLoc = new File("").getAbsolutePath();

		//HTTP url runs on port 5001
		NetworkListener nw = new NetworkListener("OAuthDemo", InetAddress.getLocalHost().getHostName(), 5001);
		
		//Configuring keys for HTTPS connection
		SSLContextConfigurator sslCon=new SSLContextConfigurator();
		sslCon.setKeyStoreFile(serverLoc + File.separator + "src" + File.separator + "com" + File.separator + "one00bytes"
				+ File.separator + "resource" + File.separator + "keystore" + File.separator
				+ "serverstore" + File.separator + "serverstore");
		sslCon.setKeyPass("welcome1".toCharArray());
		sslCon.setKeyStoreType("JKS");
		SSLEngineConfigurator sslEngineConfig = new SSLEngineConfigurator(sslCon).setClientMode(false);
		nw.setSecure(true);
		nw.setSSLEngineConfig(sslEngineConfig);
		
		HttpServer server = new HttpServer();
		server.addListener(nw);
		
		//Flows used in this project
		ServerConfiguration serverConfig = server.getServerConfiguration();
		serverConfig.addHttpHandler(new AuthzCodeFlow(), "/authzcode");
		serverConfig.addHttpHandler(new AuthzCodeFlowRedirector(), "/authzcoderedirector");
		serverConfig.addHttpHandler(new ImplicitFlow(), "/implicit");
		serverConfig.addHttpHandler(new ImplicitFlowRedirector(), "/implicitredirector");
		serverConfig.addHttpHandler(new ClientCredentialFlow(), "/clientcredential");
		serverConfig.addHttpHandler(new ResourceOwnerFlow(), "/password");
		serverConfig.addHttpHandler(new RefreshTokenFlow(), "/refreshToken");
		serverConfig.addHttpHandler(new TokenIntrospectFlow(), "/tokenIntrospect");
		serverConfig.addHttpHandler(new TokenRevokeFlow(), "/tokenRevoke");
		serverConfig.addHttpHandler(new ClientSecretJwtAuthMode(), "/client_secret_jwt");
		serverConfig.addHttpHandler(new ClientSecretPostAuthMode(), "/client_secret_post");
		serverConfig.addHttpHandler(new ClientSecretBasicAuthMode(), "/client_secret_basic");
		serverConfig.addHttpHandler(new ClientSecretPrivateKeyAuthMode(), "/private_key_jwt");
		//Index.html file
		serverConfig.addHttpHandler(new StaticHttpHandler(serverLoc + File.separator 
				+ "public" + File.separator), "/public");
		
		server.start();
		System.in.read();
	}

}
