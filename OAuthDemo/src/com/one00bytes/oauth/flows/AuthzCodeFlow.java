package com.one00bytes.oauth.flows;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URL;

import org.apache.http.client.utils.URIBuilder;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;


/**
 * Main class to cater authorization code flow
 * 
 * @author 100bytes.com
 * WEBSITE   : http://100bytes.com
 */

public class AuthzCodeFlow extends HttpHandler	{

	
	@Override
	public void service(Request req, Response res) throws Exception {
		
		/**
		 * This method creates HTML file with sign-in URL, so that when user clicks sing-in URL,
		 * which will forward to OAuth server, where real authentication happens 
		 */
		String serverLoc = new File("").getAbsolutePath();
		File f = new File(serverLoc + File.separator + "public" + File.separator + "token.html");
		BufferedReader br = new BufferedReader(new FileReader(f));
		String s = null;
		StringBuffer sb = new StringBuffer();
		while((s = br.readLine()) != null)	{
			if (s.contains("##FLOW_NAME##"))	{
				sb.append(s.replaceAll("##FLOW_NAME##", "AUHTORIZATION CODE FLOW"));
			}
			else if (s.contains("##LOGIN_URL##"))	{
				sb.append(s.replaceAll("##LOGIN_URL##", getSigninUrl(req, res)));
			}
			else	{
				sb.append(s);
			}
		}
		res.getWriter().write(sb.toString());
		br.close();
	}
	
	/**
	 * Helper method to build sign-in URL to hit the OAuth server
	 * @param req
	 * @param res
	 * @return
	 * @throws Exception
	 */
	private String getSigninUrl(Request req, Response res) throws Exception	{
		
		URIBuilder uriBuilder = new URIBuilder(OAUTH_2_0._AUTH_URL);
		
		String state = "abcde12345";
		uriBuilder.addParameter("state", state);
		
		URL url1 = new URL(req.getRequestURL().toString());
		URL redirectUrl = new URL(url1, "authzcoderedirector");
		uriBuilder.addParameter("redirect_uri", redirectUrl.toString()); //To receive redirect request from OAuth server
		uriBuilder.addParameter("response_type", "code");
		uriBuilder.addParameter("scope", OAUTH_2_0._SCOPE);
		uriBuilder.addParameter("client_id", OAUTH_2_0._CLIENT_ID_POSTAUTH);
		
		return uriBuilder.toString();
	}
}

