package com.one00bytes.oauth.flows;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;


/**
 * Demonstrates refresh token flow
 * 
 * @author 100bytes.com
 * WEBSITE: http://100bytes.com
 */
public class RefreshTokenFlow extends HttpHandler {

	@Override
	public void service(Request paramRequest, Response paramResponse) throws Exception {

		int l = paramRequest.getContentLength();
		byte c[] = new byte[l];
		paramRequest.getInputStream().read(c, 0, l);
		String refreshToken = new String(c).split("&")[0].split("=")[1];
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("grant_type", "refresh_token"));
		params.add(new BasicNameValuePair("client_id", OAUTH_2_0._CLIENT_ID_POSTAUTH));
		params.add(new BasicNameValuePair("client_secret", OAUTH_2_0._CLIENT_SECRET_POSTAUTH));
		params.add(new BasicNameValuePair("scope", OAUTH_2_0._SCOPE));
		params.add(new BasicNameValuePair("refresh_token", refreshToken)); //Refresh token passed
		byte[] b = new byte[(int) new UrlEncodedFormEntity(params).getContentLength()];
		new UrlEncodedFormEntity(params).getContent().read(b);
		
		//Hitting OAuth server to refresh the token
		Client webClient = ClientBuilder.newClient();
		javax.ws.rs.core.Response response = webClient.target(OAUTH_2_0._TOKEN_URL).request()
				.post(Entity.entity(new String(b), MediaType.APPLICATION_FORM_URLENCODED_TYPE));
		String postResponse = response.readEntity(String.class);
		
		String accessToken = OAUTH_2_0.parseToken(postResponse, OAUTH_2_0.TOKEN_TYPE.ACCESS_TOKEN, OAUTH_2_0._CLIENT_ID_POSTAUTH);
		//NO ID TOKEN
		
		String html = OAUTH_2_0.buildHtmlPage(OAUTH_2_0._TOKEN_URL, "REFRESH TOKEN FLOW",
				new String(b), postResponse, accessToken, "\"NOT_APPLICABLE\"");
		paramResponse.getWriter().write(html);	
	}

}
