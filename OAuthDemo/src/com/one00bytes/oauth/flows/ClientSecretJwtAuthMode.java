package com.one00bytes.oauth.flows;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.keys.HmacKey;



/**
 * Demonstrates client_secret_jwt authentication mode using resource owner flow
 * 
 * @author 100bytes.com
 * WEBSITE: http://100bytes.com
 */
public class ClientSecretJwtAuthMode extends HttpHandler	{

	@Override
	public void service(Request req, Response res) throws Exception {

		String claims = OAUTH_2_0.getJwt(OAUTH_2_0._CLIENT_ID_JWTAUTH);
		HmacKey signKey = new HmacKey(OAUTH_2_0._CLIENT_SECRET_JWTAUTH.getBytes());
		//Secret key us used to sign JWT which contains claims
		String clientAssertion = OAUTH_2_0.getSignedJwt(claims, signKey, AlgorithmIdentifiers.HMAC_SHA256);
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("grant_type", "password"));
		params.add(new BasicNameValuePair("client_id", OAUTH_2_0._CLIENT_ID_JWTAUTH));
		params.add(new BasicNameValuePair("client_assertion_type", "urn:ietf:params:oauth:client-assertion-type:jwt-bearer"));
		params.add(new BasicNameValuePair("client_assertion", clientAssertion));//Signed claims is passed as assertion
		params.add(new BasicNameValuePair("username", OAUTH_2_0._USERNAME));
		params.add(new BasicNameValuePair("password", OAUTH_2_0._PASSWORD));
		params.add(new BasicNameValuePair("scope", OAUTH_2_0._SCOPE));
		byte[] b = new byte[(int) new UrlEncodedFormEntity(params).getContentLength()];
		new UrlEncodedFormEntity(params).getContent().read(b);
		
		//Hitting OAuth server to get the access and ID token
		Client webClient = ClientBuilder.newClient();
		javax.ws.rs.core.Response response = webClient.target(OAUTH_2_0._TOKEN_URL).request()
				.post(Entity.entity(new String(b), MediaType.APPLICATION_FORM_URLENCODED_TYPE));
		String postResponse = response.readEntity(String.class);
		
		String accessToken = OAUTH_2_0.parseToken(postResponse, OAUTH_2_0.TOKEN_TYPE.ACCESS_TOKEN, OAUTH_2_0._CLIENT_ID_JWTAUTH);
		String idToken = OAUTH_2_0.parseToken(postResponse, OAUTH_2_0.TOKEN_TYPE.ID_TOKEN, OAUTH_2_0._CLIENT_ID_JWTAUTH);
		
		String html = OAUTH_2_0.buildHtmlPage(OAUTH_2_0._TOKEN_URL, "CLIENT_SECRET_JWT AUTH MODE",
				new String(b), postResponse, accessToken, idToken);
		res.getWriter().write(html);
		
	}
}
