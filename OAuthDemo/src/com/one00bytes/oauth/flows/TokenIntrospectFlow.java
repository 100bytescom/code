package com.one00bytes.oauth.flows;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;


/**
 * Introspect the received token against OAuth server
 * 
 * @author 100bytes.com
 * WEBSITE: http://100bytes.com
 */
public class TokenIntrospectFlow extends HttpHandler {

	@Override
	public void service(Request paramRequest, Response paramResponse) throws Exception {

		int l = paramRequest.getContentLength();
		byte c[] = new byte[l];
		paramRequest.getInputStream().read(c, 0, l);
		String checkAccessToken = new String(c).split("&")[0].split("=")[1];
		
		//Encoded clientId:clientSecret
		String cred = OAUTH_2_0._CLIENT_ID_INTROSPECTAUTH + ":" + OAUTH_2_0._CLIENT_SECRET_INTROSPECTAUTH;
		byte encoded[] = Base64.getEncoder().encode(cred.getBytes());
		cred = "Basic " + new String(encoded);
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("token", checkAccessToken)); //Token is passed for introspection
		byte[] b = new byte[(int) new UrlEncodedFormEntity(params).getContentLength()];
		new UrlEncodedFormEntity(params).getContent().read(b);
		
		//Hitting OAuth server to introspect token
		Client webClient = ClientBuilder.newClient();
		javax.ws.rs.core.Response response = webClient.target(OAUTH_2_0._INTROSPECT_URL)
				.request().header("Authorization", cred) //Passed encoded cred as authorization header 
				.post(Entity.entity(new String(b), MediaType.APPLICATION_FORM_URLENCODED_TYPE));
		String postResponse = response.readEntity(String.class);
		
		String html = OAUTH_2_0.buildHtmlPage(OAUTH_2_0._INTROSPECT_URL, "TOKEN INTROSPECT FLOW",
				new String(b), postResponse, "\"NOT_APPLICABLE\"", "\"NOT_APPLICABLE\"");
		paramResponse.getWriter().write(html);
	}

}
