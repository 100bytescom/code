package com.one00bytes.oauth.flows;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;



/**
 * Demonstrates client_secret_basic authentication mode using resource owner flow
 * 
 * @author 100bytes.com
 * WEBSITE: http://100bytes.com
 */
public class ClientSecretBasicAuthMode extends HttpHandler	{

	@Override
	public void service(Request req, Response res) throws Exception {

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("grant_type", "password"));
		params.add(new BasicNameValuePair("client_id", OAUTH_2_0._CLIENT_ID_BASICAUTH));
		//Secret key is not passed in POST body but as authorization header
		params.add(new BasicNameValuePair("username", OAUTH_2_0._USERNAME));
		params.add(new BasicNameValuePair("password", OAUTH_2_0._PASSWORD));
		params.add(new BasicNameValuePair("scope", OAUTH_2_0._SCOPE));
		byte[] b = new byte[(int) new UrlEncodedFormEntity(params).getContentLength()];
		new UrlEncodedFormEntity(params).getContent().read(b);
		
		//Basic auth mode -> Client ID and secret has to be encoded in base64 and passed as header
		String cred = OAUTH_2_0._CLIENT_ID_BASICAUTH + ":" + OAUTH_2_0._CLIENT_SECRET_BASICAUTH;
		cred = new String(Base64.getEncoder().encode(cred.getBytes()));

		//Hitting OAuth server to get the access and ID token
		Client webClient = ClientBuilder.newClient();
		javax.ws.rs.core.Response response = webClient.target(OAUTH_2_0._TOKEN_URL)
				.request()
				.header("Authorization", "Basic " + cred) 
				.post(Entity.entity(new String(b), MediaType.APPLICATION_FORM_URLENCODED_TYPE));
		String postResponse = response.readEntity(String.class);
		
		String accessToken = OAUTH_2_0.parseToken(postResponse, OAUTH_2_0.TOKEN_TYPE.ACCESS_TOKEN, OAUTH_2_0._CLIENT_ID_BASICAUTH);
		String idToken = OAUTH_2_0.parseToken(postResponse, OAUTH_2_0.TOKEN_TYPE.ID_TOKEN, OAUTH_2_0._CLIENT_ID_BASICAUTH);
		
		String html = OAUTH_2_0.buildHtmlPage(OAUTH_2_0._TOKEN_URL, "CLIENT_SECRET_BASIC AUTH MODE",
				new String(b), postResponse, accessToken, idToken);
		res.getWriter().write(html);
		
	}
}
