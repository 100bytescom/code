package com.one00bytes.oauth.flows;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;


/**
 * Redirector to receive request from OAuth server to complete
 * Authorization code flow 
 * 
 * @author 100bytes.com
 * WEBSITE   : http://100bytes.com
 */
public class AuthzCodeFlowRedirector extends HttpHandler	{

	@Override
	public void service(Request req, Response res) throws Exception {
		
		URL url1 = new URL(req.getRequestURL().toString());
		URL redirectUrl = new URL(url1, "authzcoderedirector");
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("grant_type", "authorization_code"));
		params.add(new BasicNameValuePair("code", req.getParameter("code"))); //Code received from previous response
		params.add(new BasicNameValuePair("client_id", OAUTH_2_0._CLIENT_ID_POSTAUTH));
		params.add(new BasicNameValuePair("client_secret", OAUTH_2_0._CLIENT_SECRET_POSTAUTH));
		params.add(new BasicNameValuePair("redirect_uri", redirectUrl.toString()));
		byte[] b = new byte[(int) new UrlEncodedFormEntity(params).getContentLength()];
		new UrlEncodedFormEntity(params).getContent().read(b);
		
		//Hitting OAuth server to get the access and ID token
		Client webClient = ClientBuilder.newClient();
		javax.ws.rs.core.Response response = webClient.target(OAUTH_2_0._TOKEN_URL).request()
				.post(Entity.entity(new String(b), MediaType.APPLICATION_FORM_URLENCODED_TYPE));
		String postResponse = response.readEntity(String.class);
		
		String accessToken = OAUTH_2_0.parseToken(postResponse, OAUTH_2_0.TOKEN_TYPE.ACCESS_TOKEN, 
				OAUTH_2_0._CLIENT_ID_POSTAUTH);
		String idToken = OAUTH_2_0.parseToken(postResponse, OAUTH_2_0.TOKEN_TYPE.ID_TOKEN, 
				OAUTH_2_0._CLIENT_ID_POSTAUTH);

		String html = OAUTH_2_0.buildHtmlPage(OAUTH_2_0._TOKEN_URL, "AUHTORIZATION CODE FLOW",
				new String(b), postResponse, accessToken, idToken);
		res.getWriter().write(html);
		
	}
}
