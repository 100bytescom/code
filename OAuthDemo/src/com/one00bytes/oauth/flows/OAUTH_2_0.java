package com.one00bytes.oauth.flows;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.security.Key;
import java.util.List;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import org.jose4j.jwk.HttpsJwks;
import org.jose4j.jwk.JsonWebKey;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.keys.resolvers.JwksVerificationKeyResolver;
import org.jose4j.lang.JoseException;


/**
 * Utility class
 * 
 * @author 100bytes.com
 * WEBSITE   : http://100bytes.com
 */

public class OAUTH_2_0 {
	
	//Keys used for private_key_jwt authentication of OAuth client
	public static final String PRIVATE_KEY = 
			"MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCXTe87eqv7r7/aGkYpR40Ch1tyNGcwK6NNLE92nrloh6I6R2Gfvu2ZdWvZk2XcW5o81EYcnDT9FdX7/z8Pam52vtbFb+8Ln9au+SB0jZAxFxCBWp/MnVufELtVifJf1lkELINsWdZXEnawkcosCOx+M5YVTX/lPykcV0AfHZJhYypIRl+7qsOizM2YX91nqtJqJYslo8CYcsb7Y9iN8KSUL76m0HWpQ1dyLNgZ8WWgP0KrkUpJYPrjyLrrO7iSJje2og4K1udAylaMpNGfZHAphjoQ2z0/ndm+DdVBq+ATWpT64pKslMi/iqgfCvEuPmOutaFTCFY8J0HH1ePPI7JpAgMBAAECggEAeFOTWLMDCQLIyVOxzmxHmOY6dEV2dpmNofcGLuilvJp5Cm3wFlGJtUvrfDm7fRYgQBX0uBYGEeuGpIRmCwVyWd8FADk3WwA/hCE9jVNzg4a7KpP+l4Pg9EwjHzC4Xz0gx6/6yHVdfxuA5oTaylBCe07dIY3jk1W6hP5iURx8aBNoLPH46EG1HG1EFV1nHEzXYc8+plK1T46SbdnxHNb+VSo6sm3paGWCSvUV44oYp/tpmFp5MFRHhIeGupiu2wlpfZKuYroovRNJEwEvP/tGkzulvuWL0HmT2ziB/adK61M74yeF4wRe6yLBp1FBiz2I73RHEyseJOZRpPfskYqlgQKBgQD/imiq3/t8QcRC8PWx1yHehtxpvrV+g7GpRobxBaghlJ9rvNPctsy95UonC7hpM+ptjwUjgo98IRGEYuDdEgjTI4Ru3A4grwv56UBUdWZiIQGOeesPk0+QT0z3HinofbwSd4KzH3loM01rv1yJL1btdO7lLj6HDUC63NEYgbmLOQKBgQCXk49GZk70tlDTnH0IwYBVf2A2vwILeFAdSXL0z2QxqFJmqordW4/vw/hLbTUwLwgEsZE8yt5JTRY4rbKUokP3xIrs07nZ6TVjwQtE4iY4mMSZEb4UP9IxTUC8jP9v6p9nGSs0E4rqWg+9EEHf/o4Vzm/YzBMfMShc070eNP3wsQKBgH2hsmuvypgATVkL1f8k4UXhz6M9W0VjwYiTX+xG963qv6XcI9yzEl2ZyKcBoBw4pdxPIBuhKKHCMDaB1kNZhnUnVQibr2ZqKN0oy7O6NCeySm2diu8RYWB1XEVx0vC2QCxkDSG7K5kK/9klL31ShM4hWwHZhmwnG9fCkBoYTPwhAoGAEefunwRrA2GePsFXy/b/8BmyBgWeejhmTm9k0yLE1VDrGxG+gdPVNx/RbyehhuWnbtaDL8lPZR1kKKHsXL6mhNiFCOEiLWntUrF1YVfN6BWz89kyuTWo4QYdRajqbBAtcIJ6phZA7NpizZTkE1AA/PZ8JAJHqAGooP6qcq0T6qECgYEAzFzeidhYpeNg/t9egfp5C6yuc5M1zjqYUfyhLTMfTLiyEgYo6i13rkhUbLxiJ8xWRKdixmxyEFO5z+RNs1ZWHJAUVt8TaQMDOrnL3pfBfYDV1CklmtIvn7t6fjIfDz0mk1rSg7v9F8HpOr0enQg+oBTtita5W+3U6BJhdToOjK8=";
	public static final String PUBLIC_KEY = 
			"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAl03vO3qr+6+/2hpGKUeNAodbcjRnMCujTSxPdp65aIeiOkdhn77tmXVr2"
			+ "ZNl3FuaPNRGHJw0/RXV+/8/D2pudr7WxW/vC5/WrvkgdI2QMRcQgVqfzJ1bnxC7VYnyX9ZZBCyDbFnWVxJ2sJHKLAjsfjOWFU1/5T"
			+ "8pHFdAHx2SYWMqSEZfu6rDoszNmF/dZ6rSaiWLJaPAmHLG+2PYjfCklC++ptB1qUNXcizYGfFloD9Cq5FKSWD648i66zu4kiY3tqI"
			+ "OCtbnQMpWjKTRn2RwKYY6ENs9P53Zvg3VQavgE1qU+uKSrJTIv4qoHwrxLj5jrrWhUwhWPCdBx9XjzyOyaQIDAQAB";
	public static final String _USERNAME = "100bytesUser";
	public static final String _PASSWORD = "secret";
	//Credential for POST authentication of OAuth client
	public static final String _CLIENT_ID_POSTAUTH = "qqd2ow54kyt2u";
	public static final String _CLIENT_SECRET_POSTAUTH = "KWaClReFpOCZSR2eCtzZ9_IUERHQvC-3QitwdGMLbv0";
	//Credential for BASIC authentication of OAuth client
	public static final String _CLIENT_ID_BASICAUTH = "2wsdoefgqkbji";
	public static final String _CLIENT_SECRET_BASICAUTH = "DyJGYOoPPw3jYQmPEmk87ubWuyTgwpGjyiY1E-0SZuQ";
	//Credential for JWT authentication of OAuth client
	public static final String _CLIENT_ID_JWTAUTH = "waytiz4p4gf3c";
	public static final String _CLIENT_SECRET_JWTAUTH = "3Y-jCGweW65hwnmvfKnO9gFQfephDAB-Gav1-eFuCzU";
	//Credential for PRIVATE KEY authentication of OAuth client, no secret required for this type of authentication
	public static final String _CLIENT_ID_PRIVATEAUTH = "o6lpp73ggktpu";
	//Credential to introspect OAuth token
	public static final String _CLIENT_ID_INTROSPECTAUTH = "5a3gmgwz22cyi";
	public static final String _CLIENT_SECRET_INTROSPECTAUTH = "UCNNmUCHtg53Z4qD9fFZjRwOZEokGqP96L6bAhy5jok";
	//URL's hosted in OAuth server
	public static final String _ISSUER = "http://127.0.0.1:8080/c2id";
	public static final String _AUTH_URL = "http://127.0.0.1:8080/c2id-login";
	public static final String _TOKEN_URL = "http://127.0.0.1:8080/c2id/token";
	public static final String _INTROSPECT_URL = "http://127.0.0.1:8080/c2id/token/introspect";
	public static final String _REVOKE_URL = "http://127.0.0.1:8080/c2id/token/revoke";
	public static final String _PROTECTED_RESOURCE = "http://127.0.0.1:8080/c2id/userinfo";
	public static final String _SIGN_KEYS = "http://127.0.0.1:8080/c2id/jwks.json";
	//Scopes must approved user for OAuth client to use
	public static final String _SCOPE = "openid email profile";
	
	/**
	 * Token types
	 * @author 100bytes.com
	 * WEBSITE   : http://100bytes.com
	 */
	public enum TOKEN_TYPE	{
		ACCESS_TOKEN("access_token"),
		ID_TOKEN("id_token");
		
		private String token;
		TOKEN_TYPE(String token)	{
			this.token = token;
		}
		public String getTokenType() {
			return token;
		}
	}
	
	
	/**
	 * Method to extract token based on tokenType from json string.
	 * Before returning, it will do validation of token by signature verification using OAuth server keys
	 * @param jsonString
	 * @param tokenType
	 * @param clientId
	 * @return
	 * @throws Exception
	 */
	public static String parseToken(String jsonString, TOKEN_TYPE tokenType, String clientId) throws Exception	{
		
		JsonFactory jsonFactory = new JsonFactory();
		JsonParser parser = jsonFactory.createParser(jsonString);
		String token = null;
		//Extracting token from json string by parsing it
		while(!parser.isClosed()) {
			JsonToken nextToken = parser.nextToken();
			if (JsonToken.FIELD_NAME.equals(nextToken))	{
				if (parser.getCurrentName().equals(tokenType.getTokenType()))	{
					nextToken = parser.nextToken();
					token = parser.getValueAsString();
					break;
				}
			}
		}
		
		//Setting OAuth server keys to verify signature of OAuth token
		HttpsJwks jwks = new HttpsJwks(_SIGN_KEYS);
		List<JsonWebKey> jwksList = jwks.getJsonWebKeys();
		
		JwksVerificationKeyResolver keys = new JwksVerificationKeyResolver(jwksList);
		JwtConsumerBuilder consumerBuilder = new JwtConsumerBuilder().setVerificationKeyResolver(keys);
		if (TOKEN_TYPE.ID_TOKEN.equals(tokenType))	{
			consumerBuilder.setExpectedAudience(clientId).build(); //Setting audience as client ID
		}
		JwtConsumer consumer = consumerBuilder.build();
		if (token != null)	{
			JwtClaims claims = consumer.processToClaims(token);
			return claims.toJson();
		}
		else
		{
			return "NO_TOKEN";
		}
	}
	
	
	/**
	 * Generate JWT with claims
	 * @param clientId
	 * @return
	 */
	public static String getJwt(String clientId)	{
		JwtClaims claims = new JwtClaims();
		claims.setIssuer(clientId);
		claims.setAudience(_ISSUER, _TOKEN_URL);
		claims.setSubject(clientId);
		claims.setGeneratedJwtId();
		claims.setIssuedAtToNow();
		claims.setExpirationTimeMinutesInTheFuture(10);
		return claims.toJson();
	}
	
	
	/**
	 * Sign JWT using passed key
	 * 
	 * @param claims
	 * @param signKey
	 * @param algo
	 * @return
	 * @throws JoseException
	 */
	public static String getSignedJwt(String claims, Key signKey, String algo) throws JoseException	{
		
		JsonWebSignature jws = new JsonWebSignature();
		jws.setKey(signKey);
		jws.setPayload(claims);
		jws.setAlgorithmHeaderValue(algo);
		String signedJwt = jws.getCompactSerialization();
		return signedJwt;
		
	}
	
	
	/**
	 * Substiute the place holders in the predefined HTML page
	 * 
	 * @param url
	 * @param flowName
	 * @param request
	 * @param postResponse
	 * @param accessToken
	 * @param idToken
	 * @return
	 * @throws IOException
	 */
	public static String buildHtmlPage(String url, String flowName, String request, String postResponse, 
			String accessToken, String idToken) throws IOException	{
		
		String serverLoc = new File("").getAbsolutePath();
		File f = new File(serverLoc + File.separator + "public" + File.separator + "redirector.html");
		BufferedReader br = new BufferedReader(new FileReader(f));
		String s = null;
		StringBuffer sb = new StringBuffer();
		while((s = br.readLine()) != null)	{
			if (s.contains("##URL_TO_HIT##"))	{
				sb.append(s.replaceAll("##URL_TO_HIT##", url));
			}
			else if (s.contains("##FLOW_NAME##"))	{
				sb.append(s.replaceAll("##FLOW_NAME##", flowName));
			}
			else if (s.contains("##POST_REQUEST##"))	{
				sb.append(s.replaceAll("##POST_REQUEST##", request));
			}
			else if (s.contains("##POST_RESPONSE##"))	{
				sb.append(s.replaceAll("##POST_RESPONSE##", postResponse));
			}
			else if (s.contains("##ACCESS_TOKEN##"))	{
				sb.append(s.replaceAll("##ACCESS_TOKEN##", accessToken));
			}
			else if (s.contains("##ID_TOKEN##"))	{
				sb.append(s.replaceAll("##ID_TOKEN##", idToken));
			}
			else	{
				sb.append(s);
			}
		}
		br.close();
		return sb.toString();
	}
}
