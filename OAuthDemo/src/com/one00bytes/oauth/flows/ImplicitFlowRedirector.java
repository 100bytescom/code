package com.one00bytes.oauth.flows;

import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;



/**
 * Recieve call back from OAuth server for implicit flow
 * 
 * @author 100bytes.com
 * WEBSITE: http://100bytes.com
 */
public class ImplicitFlowRedirector  extends HttpHandler	{

	@Override
	public void service(Request req, Response res) throws Exception {
	
		//All the tokens are received in browser address bar
		String html = OAUTH_2_0.buildHtmlPage(OAUTH_2_0._TOKEN_URL, "IMPLICIT FLOW", "NO_POST_REQUEST", 
				"\"NO_POST_RESPONSE\"", "\"CHECK_BROWSER_ADDRESS_BAR\"", "\"CHECK_BROWSER_ADDRESS_BAR\"");
		res.getWriter().write(html);
		
	}
	
}
