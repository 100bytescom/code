package com.one00bytes.oauth.flows;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;


/**
 * To revoke the access token issued by OAuth server
 * 
 * @author 100bytes.com
 * WEBSITE: http://100bytes.com
 */
public class TokenRevokeFlow extends HttpHandler {

	@Override
	public void service(Request paramRequest, Response paramResponse) throws Exception {

		int l = paramRequest.getContentLength();
		byte c[] = new byte[l];
		paramRequest.getInputStream().read(c, 0, l);
		String checkAccessToken = new String(c).split("&")[0].split("=")[1]; //Extracting token
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("token", checkAccessToken));
//		params.add(new BasicNameValuePair("token_type_hint", "access_token")); //OPTIONAL
		params.add(new BasicNameValuePair("client_id", OAUTH_2_0._CLIENT_ID_POSTAUTH));
		params.add(new BasicNameValuePair("client_secret", OAUTH_2_0._CLIENT_SECRET_POSTAUTH));
		byte[] b = new byte[(int) new UrlEncodedFormEntity(params).getContentLength()];
		new UrlEncodedFormEntity(params).getContent().read(b);
		
		//Hitting OAuth server to revoke token
		Client webClient = ClientBuilder.newClient();
		javax.ws.rs.core.Response response = webClient.target(OAUTH_2_0._REVOKE_URL).request()
				.post(Entity.entity(new String(b), MediaType.APPLICATION_FORM_URLENCODED_TYPE));
		response.readEntity(String.class);
		
		String html = OAUTH_2_0.buildHtmlPage(OAUTH_2_0._REVOKE_URL, "TOKEN REVOKE FLOW",
				new String(b), "\"NO_RESPONSE_RETURNED\"", "\"NOT_APPLICABLE\"", "\"NOT_APPLICABLE\"");
		paramResponse.getWriter().write(html);
	}

}
