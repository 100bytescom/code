package com.one00bytes.references;

import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;


class StrongReferent	{
    protected void finalize() {
    	System.out.println("STRONG References :: Created object count :: " +  References.count);
        System.out.println("STRONG References :: Final PROCESS completed");
    }
	
}

class SoftReferent	{
    protected void finalize() {
    	System.out.println("SOFT References   :: Created object count :: " +  References.count);
        System.out.println("SOFT References   :: Final PROCESS completed");
    }
	
}

class WeakReferent	{
    protected void finalize() {
    	System.out.println("WEAK References   :: Created object count :: " +  References.count);
        System.out.println("WEAK References   :: Final PROCESS completed");
    }
	
}

public class References {
    
    public static void initiateGC() throws InterruptedException {
        System.out.println("Initiating GC...");
        System.gc();
//        Thread.sleep(5000);        
    }
    
    public static long count = 0;
 
    public static void main(String args[]) throws InterruptedException {
        System.out.println("Creating strong references");
 
        StrongReferent strongReferences = new StrongReferent();
        
        SoftReferent softStrReferences = new SoftReferent(); //STRONG References
        SoftReference softReferences = new SoftReference(softStrReferences);
        
        WeakReferent weakStrReferences = new WeakReferent(); //STRONG References
        WeakReference weakReferences = new WeakReference(weakStrReferences);
 
        References.initiateGC();
        System.out.println("Invalidating strong references");
        softStrReferences = null;
        weakStrReferences = null;
        strongReferences = null;
        References.initiateGC();

        System.out.println("Eating heap....");
        try
        {
            List eatHeap = new ArrayList();
            while(true) {
            	count++;
                eatHeap.add(new References());
            }
        }
        catch (OutOfMemoryError e) {
            System.out.println("No space. OutOfMemory Raised");
        }
 
        System.out.println("Total Created Objects "  + count);
        System.out.println("Completed Reference test");
    }
 
}
