package com.ashok.references;

import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;


class WeakHashMapReferent	{
	protected void finalize() {
        System.out.println("===========Done with WeakHashMap Referent=============");
    }
}

public class WeakHashMaps {

    public static void cleanUp() throws InterruptedException {
        System.out.println("Running GC and sleeping");
        System.gc();
        Thread.sleep(2000);
    }
 
    public static void main(String args[]) throws InterruptedException {
 
    	System.out.println("Creating weak hash map with strong reference");
        WeakHashMapReferent weakStrong = new WeakHashMapReferent();
        Map<weakhashmapreferent,string> imagesHashmap = new WeakHashMap<weakhashmapreferent,string>();
        imagesHashmap.put(weakStrong, "Huge image");
        
        System.out.println("Creating hash map with strong reference");
        WeakHashMapReferent strongStrong = new WeakHashMapReferent();
        Map<weakhashmapreferent,string> imagesWeakHashMap = new HashMap<weakhashmapreferent,string>();
        imagesWeakHashMap.put(strongStrong, "Huge image");
 
        WeakHashMaps.cleanUp();
        System.out.println("\tDoes Weak hash map has image? " + (imagesHashmap.size() == 1));
        System.out.println("\tDoes  hash map has image?     " + (imagesWeakHashMap.size() == 1));
        
        System.out.println("Invalidating references");
        weakStrong = null;
        strongStrong = null;
        WeakHashMaps.cleanUp();
 
        System.out.println("\tDoes Weak hash map has image? " + (imagesHashmap.size() == 1));
        System.out.println("\tDoes  hash map has image?     " + (imagesWeakHashMap.size() == 1));
 
        System.out.println("Finished");
    }
}

