package com.one00bytes.lambda;


@FunctionalInterface
interface MathOperation	{
	int operation(Integer a, Integer b);
}

@FunctionalInterface
interface NoParameter	{
	void printBanner();
}

@FunctionalInterface
interface SingleParam	{
	int Squareit(int no);
}

@FunctionalInterface
interface Employees	{
	void processEmps(Employee emp);
}

class Employee	{
	
	int no;
	String name;
	
	Employee(int no, String name)	{
		this.no = no;
		this.name = name;
	}

	public int getNo() {
		return no;
	}

	public String getName() {
		return name;
	}
}

public class LambdaExpression {

	public static void main(String[] args) {
		
		//ZERO parameter		
		printBanner(
				() -> System.out.println("=========================")
				);
		
		//SINGLE Parameter with Single statement which returns value without return statement		
		squareIt( no -> no * no);
		
		//More than one parameter - Needed parenthesis
		System.out.print("MORE than one PARAMETER in expression :");
		lambdaOperation(
				(op1, op2) -> op1 * op2 
			);

		//More than one statement - Needed CURLY BRACES and RETURN statement
		lambdaOperation(
				(op1, op2) -> {
					System.out.println("Entered Values :: " + op1 + "<-->" + op2);
					System.out.print("MORE than one STATEMENT in expression :");
					return(op1 - op2); 
				}
			);
		
		//As an object
		MathOperation addition = (a, b) -> a + b;
		System.out.println("Lambda expression as an object: " + addition.operation(10, 20));

		//With non-primitive type
		System.out.print("Lambda expression with non-primitve object ->");
		nonPrimitive(
			emp -> System.out.println("Employee no :" + emp.getNo() + " and name is :" + emp.getName())
		);
		
		//Accessing local variables - no need to be FINAL
		int test = 11111; 
		printBanner(
				() -> System.out.println("=========================  " + test)
				);
	}

	public static void printBanner(NoParameter oper)	{
		oper.printBanner();
	}

	public static void squareIt(SingleParam no)	{
		System.out.println("SINGLE Parameter with SINGLE statement " + no.Squareit(5));
	}

	public static void lambdaOperation(MathOperation oper)	{
		System.out.println(oper.operation(10, 25));
	}
	
	public static void nonPrimitive(Employees emp)	{
		Employee employee = new Employee(100, "100bytes");
		emp.processEmps(employee);
	}

}
