package com.one00bytes.references;

import java.lang.ref.PhantomReference;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.HashMap;
import java.util.Map;
 
class PhantomReferent	{
}


public class PhantomReferences {
 
    public static void main(String args[]) throws InterruptedException {

    	PhantomReferent userStrong = new PhantomReferent(); //STRONG Reference
    	
    	ReferenceQueue cleanUpQ = new ReferenceQueue(); 
 
    	//PHANTOM reference with strong reference and reference queue
        PhantomReference phantom = new 
        		PhantomReference(userStrong, cleanUpQ); 
        
        Map<reference,string> tasks = new HashMap<reference,string>();
        tasks.put(phantom, "100bytes.com image1.bmp"); //Tracking image1.bmp with phantom for every user
 
        System.out.println("Invalidating strong reference ...");
        userStrong = null; //User logged off
 
        System.out.println("Running garbage collector");
        System.gc();
        System.out.println("Sleeping for 2 secs");
        Thread.sleep(2000);
        
        System.out.println("Check for references in reference queue"); 
        Reference reference = cleanUpQ.remove();
        if (reference != null) {
            System.out.println(tasks.remove(reference)); //As user logged off, we can remove image
        }
        System.out.println("Done");
    }
}
