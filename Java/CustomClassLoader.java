package com.one00bytes.classloader;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;


/**
 * Custom class loader loads classes in the following order
 * 1. Load from cache
 * 2. Load using parent class loader
 * 3. Load the class manually
 * @author 100bytes.com
 *
 */

class Loader extends ClassLoader	{
	
	Map<string, class=""> classesCache = new HashMap<string, class="">();
	private String fileName = null;
	private String className = null;
	
	public Loader(ClassLoader parent, String fileName)	{
		super(parent);
		this.fileName = fileName;
	}

	/**
	 * This is override method of parent class loader
	 */
	public Class loadClass(String className) throws ClassNotFoundException {
		
		this.className = className;
		
		Class requiredClass = classesCache.get(className);
		if ( requiredClass != null ) {
			System.out.println("====Loading from cache                >> " + className);
			return requiredClass;
		}
		
		try	{
			requiredClass = findSystemClass(className);
		} catch (ClassNotFoundException cfe )	{}
		if ( requiredClass != null ) {
			System.out.println("####Loading using parent class loader >> " + className);
			return requiredClass;
		}
		
		try	{
			byte classContent[] = loadClassContent();
			requiredClass = defineClass(className, classContent, 0, classContent.length, null);  
			classesCache.put(className, requiredClass);  
		} catch(Exception e)	{
			throw new ClassNotFoundException("Class Loading failed", e);
		}
		
		if ( requiredClass != null ) {
			System.out.println("----Loading class manually            >> " + className);
			return requiredClass;
		}
		return requiredClass;
	}
	
	
	/**
	 * Loads the content and return content in byte array.
	 * Automatically detects class or jar type and load accordingly
	 * @return
	 * @throws Exception
	 */
	private byte[] loadClassContent() throws Exception	{
		
		byte[] classContent = null;
		String fileType = fileName.substring(fileName.indexOf('.'));
		
		if ( fileType.equals(".class") )	{
			classContent  = readContent(new FileInputStream(fileName));
		}
		else if ( fileType.equals(".jar"))	{
			JarFile jarFile = new JarFile(fileName);
			String classPathInJar = className.replace('.', '/');
			JarEntry jarEntry = jarFile.getJarEntry(classPathInJar + ".class");
			InputStream is = jarFile.getInputStream(jarEntry);
			classContent = readContent(is);
			jarFile.close();
		}
		else	{
			System.out.println("INVALID FILE TYPE");
			System.exit(-1);
		}
		return classContent;
	}
	
	/**
	 * Read content from input stream where it is pointing to class file
	 * @param is
	 * @return
	 * @throws Exception
	 */
	private byte[] readContent(InputStream is) throws Exception	{
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		int content;
		while ( (content = is.read()) != -1)	{
			bos.write(content);
		}
		return bos.toByteArray();
	}
}


public class CustomClassLoader {
	
	public static void main(String[] args) throws Exception{
		
		//Passing parent class loader and file name
		Loader loader = new Loader(CustomClassLoader.class.getClassLoader(), args[1]);
		//Passing class name
		Class classes = loader.loadClass(args[0]);
		
		//Try call method using reflection
		Object obj = classes.getConstructor(null).newInstance(null);
		Method method = classes.getMethod("print", String.class);
		method.invoke(obj, "100bytes.com");
		
		//Try loading again
		System.out.println("\n\n------->Loading class again<-------");
		loader.loadClass(args[0]);
	}

}
