package com.one00bytes.lambda;

@FunctionalInterface
interface ExceptionTest	{
	void tester(int i) throws Exception;
}

@FunctionalInterface
interface ReturnParameterTest	{
	int Operation(int a);
}

@FunctionalInterface
interface MathOperations	{
	int operation(int oper1, int oper2);
}


public class LambdaPlacement {

	public static void exceptionTest(ExceptionTest test)	{
		try {
			test.tester(0);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static ReturnParameterTest getOperation()	{
		return(
			(a) -> a * a
		);
	}
	
	static void Overload(ExceptionTest et)	{
		try {
			System.out.println("Overloaded exception test :");
			et.tester(10);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	static void Overload(ReturnParameterTest rpt)	{
		System.out.println("Overloaded return parameter test :" + rpt.Operation(5));
	}
	
	public static void main(String[] args) {

		//EXCEPTION throw test
		exceptionTest(
			(i)->{
				System.out.print("Exception Test :: "); 
				if ( i == 0 ) {
					throw new Exception("Number shouldn't be ZERO");
				}
			}
		);
		
		//RETURN parameter test
		System.out.println("Return parameter test :: " + getOperation().Operation(5));
		
		//ARRAY initialization test
		MathOperations operations[] = new MathOperations[]{
			(a, b) -> a + b,
			(a, b) -> a - b,
			(a, b) -> a * b,
			(a, b) -> a / b,
		};
		System.out.print("Array initialization test ::");
		for(MathOperations operation : operations)	{
			System.out.print("\t" + operation.operation(20, 10));
		}
		
		//OVERLOAD function test
		Overload((a) -> a * a);
		
		//BOILER Plate code removal
		new Thread(//Anonymous Inner class
			new Runnable()	{
				@Override
				public void run() {
					System.out.println("Inside Anonymous class RUN");
				}
			}
		).start();
		new Thread( //Lambda Expression
			() -> System.out.println("Inside lamda expression RUN")
		).start();
	}

}
