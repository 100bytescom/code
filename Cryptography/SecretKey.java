import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.xml.bind.DatatypeConverter;

public class SecretKeyTest {

	public static void main(String[] args) throws NoSuchAlgorithmException {

		/**
		 * DES supported key size  - 56
		 * AES supported key sizes - 128, 192, 256
		 */
		KeyGenerator kg = KeyGenerator.getInstance("AES");//Pass DES and its supported key size
		kg.init(256);
		SecretKey sk = kg.generateKey();
		
		System.out.println("Secret Key value     :: " + DatatypeConverter.printBase64Binary(sk.getEncoded()));
		System.out.println("Secret Key algorithm :: " + sk.getAlgorithm());
		System.out.println("Secret Key format    :: "  + sk.getFormat());
		
		//Secret key generation using random number
		SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
		sr.setSeed("This is a test passphrase".getBytes());
		kg.init(256, sr);
		sk = kg.generateKey();
		System.out.println("Secret Key generated using random value :: " + DatatypeConverter.printBase64Binary(sk.getEncoded()));
	}

}
