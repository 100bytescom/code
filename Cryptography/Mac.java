import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.xml.bind.DatatypeConverter;


public class MacTest {

	public static void main(String[] args) throws Exception {
		
		KeyGenerator keyGen = KeyGenerator.getInstance("HmacMD5");
		SecretKey sk = keyGen.generateKey();
//		System.out.println(DatatypeConverter.printBase64Binary(sk.getEncoded()));
		
		Mac mc = Mac.getInstance("HmacMD5");
		mc.init(sk);
		
		mc.update("First word".getBytes());
		mc.update("Next word".getBytes());
		byte hash[] = mc.doFinal();
		
		System.out.println("MAC code      :: " + DatatypeConverter.printBase64Binary(hash));
		System.out.println("MAC algorithm :: " + mc.getAlgorithm());
		System.out.println("MAC length    :: " + mc.getMacLength());
		System.out.println("MAC provider  :: " + mc.getProvider().getName());
	}

}
