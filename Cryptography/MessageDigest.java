import java.security.MessageDigest;

import javax.xml.bind.DatatypeConverter;


public class MessageDigestTest {

	public static void main(String[] args) throws Exception {
		
		MessageDigest md =  MessageDigest.getInstance("MD5");
//                MessageDigest md =  MessageDigest.getInstance("SHA1");
		byte input[] = "Test word".getBytes();
		md.update(input);
		md.update("Next word".getBytes());
		byte hash[] = md.digest(); //Digest reset after call is made
		
		System.out.println("Message Digest :: " + DatatypeConverter.printBase64Binary(hash));
		System.out.println("Algorithm      :: " + md.getAlgorithm());
		System.out.println("Digest length  :: " + md.getDigestLength());
		System.out.println("Provider name  :: " + md.getProvider().getName());
		
		System.out.println("====Generate new message digest for the same input string and new character \"0\"====");
		md.update("Test word".getBytes());
		md.update("Next word".getBytes());
		md.update("0".getBytes()); //Adding extra character
		byte nextHash[] = md.digest();
		System.out.println("Message Digest :: " + DatatypeConverter.printBase64Binary(nextHash));
		System.out.println("Digest comparision :: " + MessageDigest.isEqual(hash, nextHash));
	}

}
