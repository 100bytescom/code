import java.security.SecureRandom;
import javax.xml.bind.DatatypeConverter;


public class SecureRandomTest {

	public static void main(String[] args) throws Exception {
		
		SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
		
		System.out.println("\n\n=======Generating seed values with Zero valued bytes ============");
		byte[] firstSeed = new byte[16];
		secureRandom.setSeed(firstSeed); //Seeding with zero valued bytes 

		byte[] genRandom = secureRandom.generateSeed(16);

		byte[] nRand = new byte[16];
		secureRandom.nextBytes(nRand);

		System.out.println("Seed value with generate Random: "+DatatypeConverter.printBase64Binary(genRandom));
		System.out.println("Seed value with next function  : "+ DatatypeConverter.printBase64Binary(nRand));
		
		
		System.out.println("\n\n=======Generating seed values using same zero valued bytes ============");
		genRandom = secureRandom.generateSeed(16);
		System.out.println("Seed value with generate Random: "+ DatatypeConverter.printBase64Binary(genRandom));
		secureRandom.nextBytes(nRand);
		System.out.println("Seed value with next function  : "+ DatatypeConverter.printBase64Binary(nRand));
		
		System.out.println("\n\n=======Generating seed values with another Zero valued bytes ============");
		byte nextSeed[] = new byte[16];
		secureRandom.setSeed(nextSeed); //Variable 'seed' or 'data' can also be used
		
		genRandom = secureRandom.generateSeed(16);

		nRand = new byte[16];
		secureRandom.nextBytes(nRand);

		
		System.out.println("Seed value with generate Random: "+ DatatypeConverter.printBase64Binary(genRandom));
		System.out.println("Seed value with next function  : "+ DatatypeConverter.printBase64Binary(nRand));
		
		System.out.println("\n\n=======Generating seed values using last seed value============");
		secureRandom.setSeed(genRandom); //Using last seed value
		
		genRandom = secureRandom.generateSeed(16);

		nRand = new byte[16];
		secureRandom.nextBytes(nRand);

		
		System.out.println("Seed value with generate Random: "+ DatatypeConverter.printBase64Binary(genRandom));
		System.out.println("Seed value with next function  : "+ DatatypeConverter.printBase64Binary(nRand));

	}

}
