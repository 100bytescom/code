This project explains JWT token
1. Signing JWT token
2. Encrypting JWT token
3. Sign and Encrypt JWT token
4. Verify JWT token using certificate
5. Verify JWT token using verification key resolver
6. Verify JWT token using keys served online i.e., JWKs