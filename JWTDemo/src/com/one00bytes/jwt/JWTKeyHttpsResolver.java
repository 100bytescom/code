package com.one00bytes.jwt;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Arrays;
import java.util.List;

import org.jose4j.jwk.HttpsJwks;
import org.jose4j.jwk.JsonWebKey;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.keys.resolvers.JwksVerificationKeyResolver;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;


/**
 * Class shows that keys can be retrieved online which is used to verify the signature of JWT token. 
 * This concept is calle Jwt web key resolver, where sender of JWT token hosted a REST endpoint,
 * which serves public key to verify signature of JWT token. Similary JWT web encryption can also be
 * acheived.
 * 
 * @author 100bytes.com
 * WEBSITE: http://100bytes.com
 */
public class JWTKeyHttpsResolver {

	public static void main(String[] args) throws Exception {

		/***************************SENDER'S END ***********************************/
		 
		JwtClaims claims = new JwtClaims();
		claims.setAudience("Admins");
		claims.setExpirationTimeMinutesInTheFuture(10); //10 minutes from now
		claims.setGeneratedJwtId();
		claims.setIssuer("CA");
		claims.setIssuedAtToNow();
		claims.setNotBeforeMinutesInThePast(2);
		claims.setSubject("100bytesAdmin");
		
		claims.setClaim("email", "100bytesAdmin@100bytes.com");
		claims.setClaim("Country", "Antartica");
		List<String> hobbies = Arrays.asList("Blogging", "Playing cards", "Games");
		claims.setStringListClaim("hobbies", hobbies);
		System.out.println(claims.toJson());
		
		//Corresponding public key is hosted in com.one00bytes.jwt.restws.jwk.RestjwkWS
		//which exposes http://localhost:7001/jwkrestws/resources/jwks/sign
		String privateKey = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCIpUzDyYitMLzTbgUBe4W5+d"
				+ "1f+xM2oA+Q2REP3M9arqeudZYhV9+LmGDzBIwnrPn0ReNa2jYIxBw0StECikHSOJiOuw168K1qUpFaH9Lgcg"
				+ "QG5+4MQzFCa+6z4CaOMuqRvkK9NCZiMQmMSfrYsGyq7h3A67H53QFlCty226zQNrQTInjdwezv1G+2sUE7f+C"
				+ "BUynWKFBbpswNwkdfMZFsooYlbFQ9ORSbTCQOslPBm62gI2P4EyW57InHwAsTSkVYN3pm4iaI8IBygTW7+vGX"
				+ "J+oOyhQrufK2OqGSeeAtbBsRINb7qsVvic8cyesKrRui4E5HUx6EmXJO6JhGtKPNAgMBAAECggEAMck4TOVy3"
				+ "NczFT2yqTWeJYJ6O+w4teBfi4DrDIKBMiMMiS3ig0eG9U+RkzmG7zXA5FVKXuG+ajc5p3vS50AEd1mTi/1vDe"
				+ "n+5QwsVU/e/1PLT9DVFLaujerRT8xNf+XhPTS7EECd84y2U9zofmbZUPXs9UtqzwTM60mn3R/wuPOMQ/uWiQZz"
				+ "uq4xwLvNUN10z1VvXvEpcKBrxwTtH3EAAbg92DvMOJp23YscxAeRgtiZHyAOirFAKhyv0O3e/stTXpWD1/RpsU"
				+ "iGB9gYa3vBwZHEhnDIwSr+fPDfD4mtb1LFzQdjnODXQaRH/NFojui/AZLsZMiKvLs6ubVv00VeaQKBgQDRXnZS"
				+ "ir/r9Qcxx5vXGBRLFaDsxJeBs7PDY/ibaXJnH9iIHSKkmUS0wQuiD8c+gHxuTPqBIbOn7TNvgn1c+z3JS8NpjC"
				+ "C6PS/lsK1UZS8HV2R2t0Nd/YxL0TBz92vTfCq5imGjQ7w85lvJPDYJZ3OZ/PYYzcTGTatQZ/52T4/qiwKBgQCn"
				+ "FGSBXJ/2HiO21lgOjQ7vUeYDpFqpSMoujs5HwYiJPSeoNw6xgGrT2wCxLZg8GntXr7Ey1KjsFZ4r/GyZwm0UnJ"
				+ "9EfkshJM+Oo2ZOnRqjUzd+1S+JJul9jfglJywv49Xr39k0Ztp7usflqLoTrjtk7vK+R7FM/JCFYmYz+CjuBwKB"
				+ "gBv/gdsv6tJnBftGMjTlZ3FTBx4ifBBAM73hVJGT3FSmEZLCMqvIQ0PYvPGDIqXdEMa/TJCQLTI7qjEzRgh1IK"
				+ "IWm581ufqtzAXvRn6OuWdF7I1jnIKpMWXCJ7SaVbUGWcBOHRh0KmwKciPLBX7kcrUY+t/+RF4wbMRn48N2iExZ"
				+ "AoGBAIq+Pm/GEPd6M8Ii4GDE9j/5zH39v2OQFITxBY9EwTLl28auYb84mn1vAXYgHKjcfMxmE0nYmqXhlcWFCl"
				+ "HsyFQzc3aXQaA2Rz11pwFlKLez5QOn2J+V9pLSj9uaWfEyy3PiISaIuqnAgzrNEChDqQK6Ak3MjwkVOpXpDmHdk"
				+ "2+FAoGAYwsS+SwmVqFiAb01nhkDKvKql/+RsdzKdyrrEobolDtUMswHm6ENteUkLbUZoe9uUhFLMGdGmiIhyEqI"
				+ "gwjk9H1p75Sha3rJU4ir5W4Oszn9LI01BjWJ/tMBBCuqas/4BHovPpXPbt63jSU06XY9NOSkz2OYqXOzZgTPyjgOpec=";
		BASE64Decoder b64d = new BASE64Decoder();
		byte[] clear = b64d.decodeBuffer(privateKey);
	    PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(clear);
	    KeyFactory fact = KeyFactory.getInstance("RSA");
	    PrivateKey priv = fact.generatePrivate(keySpec);
		
		//SIGNING
		JsonWebSignature jws = new JsonWebSignature();
		jws.setKey(priv);
		jws.setPayload(claims.toJson());
		jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);
		String signedJwt = jws.getCompactSerialization();
		System.out.println("Signed ::" + signedJwt);
		
		
		/***************************RECEIVER'S END ***********************************/ 

		//Class com.one00bytes.jwt.restws.jwk.RestjwkWS has to be hosted in a web server 
		//and which exposes a REST endpoint where corresponding public key is served
		//Signing or decryption keys are accessed from URL: http://localhost:7001/jwkrestws/resources/jwks/keys 

		HttpsJwks https_jwks = new HttpsJwks("http://localhost:7001/jwkrestws/resources/jwks/sign");
		//Can retrieve list of keys
		List<JsonWebKey> jwks_list = https_jwks.getJsonWebKeys();
		BASE64Encoder b64e = new BASE64Encoder();
		for(JsonWebKey jwk : jwks_list ){
			System.out.println(jwk.toJson());
			System.out.println(b64e.encode(jwk.getPublicKey().getEncoded()));
		}
		JwksVerificationKeyResolver jwks_resolver = new JwksVerificationKeyResolver(jwks_list);
	
		JwtConsumer consumer = new JwtConsumerBuilder()
								.setExpectedAudience("Admins")
								.setExpectedIssuer("CA")
								.setVerificationKeyResolver(jwks_resolver)
								.setRequireSubject()
								.build();
		JwtClaims receivedClaims = consumer.processToClaims(signedJwt);
		System.out.println("SUCESS :: JWT Validation");
		Utils.processClaims(receivedClaims);
	}

}
