package com.one00bytes.jwt;

import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jose4j.jwk.JsonWebKey;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.keys.resolvers.JwksVerificationKeyResolver;

import sun.misc.BASE64Encoder;


/**
 * JWT verification by key resolver
 * 
 * @author 100bytes.com
 * WEBSITE: http://100bytes.com
 */
public class JWTKeyResolver {

	public static void main(String[] args) throws Exception {

		/***************************SENDER'S END ***********************************/
		 
		JwtClaims claims = new JwtClaims();
		claims.setAudience("Admins");
		claims.setExpirationTimeMinutesInTheFuture(10); //10 minutes from now
		claims.setGeneratedJwtId();
		claims.setIssuer("CA");
		claims.setIssuedAtToNow();
		claims.setNotBeforeMinutesInThePast(2);
		claims.setSubject("100bytesAdmin");
		
		claims.setClaim("email", "100bytesAdmin@100bytes.com");
		claims.setClaim("Country", "Antartica");
		List<String> hobbies = Arrays.asList("Blogging", "Playing cards", "Games");
		claims.setStringListClaim("hobbies", hobbies);
		System.out.println(claims.toJson());
		
		//SIGNING
		RsaJsonWebKey jsonSignKey = RsaJwkGenerator.generateJwk(2048);
		JsonWebSignature jws = new JsonWebSignature();
		jws.setKey(jsonSignKey.getPrivateKey());
		jws.setPayload(claims.toJson());
		jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);
		String signedJwt = jws.getCompactSerialization();
		System.out.println("Signed ::" + signedJwt);
		BASE64Encoder b64 = new BASE64Encoder();
		System.out.println("Private Key :: " + b64.encode(jsonSignKey.getPrivateKey().getEncoded()));
		RSAPublicKey rsaPub = (RSAPublicKey) jsonSignKey.getPublicKey();
				
		
		
		/***************************RECEIVER'S END ***********************************/

		System.out.println("Public Key :: " + b64.encode(jsonSignKey.getPublicKey().getEncoded()));
		
		//Preparing verification key resolver with publiv key received from sender of JWT token
		//ADVANTAGE: Can load more than one key and used to verify JWT token signature
		RsaJsonWebKey sign_jwk = new RsaJsonWebKey(rsaPub);
		sign_jwk.setAlgorithm("RSA");
		sign_jwk.setUse("sig");
		sign_jwk.setKeyId("12345");
		List<JsonWebKey> jwks_list = new ArrayList<JsonWebKey>();
		jwks_list.add(sign_jwk);
		JwksVerificationKeyResolver jwks_resolver = new JwksVerificationKeyResolver(jwks_list);
		
		JwtConsumer consumer = new JwtConsumerBuilder()
								.setExpectedAudience("Admins")
								.setExpectedIssuer("CA")
								//Setting verification resolver(with list of keys) to verify the JWT token
								.setVerificationKeyResolver(jwks_resolver)
								.setRequireSubject()
								.build();
		JwtClaims receivedClaims = consumer.processToClaims(signedJwt);
		System.out.println("SUCESS :: JWT Validation");
		Utils.processClaims(receivedClaims);
	}

}
