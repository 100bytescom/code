package com.one00bytes.jwt;

import java.util.Collection;

import org.jose4j.jwt.JwtClaims;

/**
 * Utitlity class for JWT project
 * 
 * @author 100bytes.com
 * WEBSITE: http://100bytes.com
 */
public class Utils {

	
	public static void processClaims(JwtClaims receivedClaims)	{
		System.out.println("RECEIVED CLAIMS :: " + receivedClaims.toJson());
		Collection<String> recievedClaims = receivedClaims.getClaimNames();
		for(String c : recievedClaims )	{
			System.out.println(c + " <<>> " + receivedClaims.getClaimValue(c));
		}
	}
}
