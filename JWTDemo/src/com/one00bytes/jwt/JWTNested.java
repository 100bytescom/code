package com.one00bytes.jwt;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import org.jose4j.jwe.ContentEncryptionAlgorithmIdentifiers;
import org.jose4j.jwe.JsonWebEncryption;
import org.jose4j.jwe.KeyManagementAlgorithmIdentifiers;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.jwt.consumer.JwtContext;


/**
 * Signing and encryption of JWT token using NESTED tag
 * 
 * @author 100bytes.com
 * WEBSITE: http://100bytes.com
 */
public class JWTNested {

	public static void main(String[] args) throws Exception {
		
		/***************************SENDER'S END ***********************************/
		 
		JwtClaims claims = new JwtClaims();
		claims.setAudience("Admins");
		claims.setExpirationTimeMinutesInTheFuture(10); //10 minutes from now
		claims.setGeneratedJwtId();
		claims.setIssuer("CA");
		claims.setIssuedAtToNow();
		claims.setNotBeforeMinutesInThePast(2);
		claims.setSubject("100bytesAdmin");
		claims.setClaim("email", "100bytesAdmin@100bytes.com");
		claims.setClaim("Country", "Antartica");
		List<String> hobbies = Arrays.asList("Blogging", "Playing cards", "Games");
		claims.setStringListClaim("hobbies", hobbies);
		System.out.println(claims.toJson());
		
		//SIGNING
		RsaJsonWebKey sendersKey = RsaJwkGenerator.generateJwk(2048);
		JsonWebSignature jws = new JsonWebSignature();
		jws.setKey(sendersKey.getPrivateKey());
		jws.setPayload(claims.toJson());
		jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);
		String signedJwt = jws.getCompactSerialization();
		System.out.println("Signed ::" + signedJwt);
		
		//ENCRYPTING
		RsaJsonWebKey receiversKey = RsaJwkGenerator.generateJwk(2048);
		KeyGenerator keyGen = KeyGenerator.getInstance("AES");
		keyGen.init(128);
		SecretKey contentEncryptKey = keyGen.generateKey();
		JsonWebEncryption jwe = new JsonWebEncryption();
		jwe.setKey(receiversKey.getPublicKey());  //Got PUBLIC key from receiver who is about to receive JWT token
		jwe.setPayload(signedJwt);
		jwe.setAlgorithmHeaderValue(KeyManagementAlgorithmIdentifiers.RSA_OAEP_256);
		jwe.setContentEncryptionKey(contentEncryptKey.getEncoded());
		jwe.setEncryptionMethodHeaderParameter(ContentEncryptionAlgorithmIdentifiers.AES_128_GCM);
		jwe.setHeader("cty", "jwt"); //******IMP:: NESTED JWT
		SecureRandom iv = SecureRandom.getInstance("SHA1PRNG");
		jwe.setIv(iv.generateSeed(32));
		String encryptedJwt = jwe.getCompactSerialization();
		System.out.println("Encrypted ::" + encryptedJwt);
		
		
		/***************************RECEIVER'S END ***********************************/ 

		JwtConsumer consumer = new JwtConsumerBuilder()
								.setExpectedAudience("Admins")
								.setExpectedIssuer("CA")
								//Below 2 lines disables signature verification, if uncommented
								//Else signature verification done by default
								//If signature verification is disabled, then signature verification can be done like below
						        /*.setDisableRequireSignature()
						        .setSkipSignatureVerification() */  
						        .setVerificationKey(sendersKey.getPublicKey()) //Received PUBLIC key from sender
								.setDecryptionKey(receiversKey.getPrivateKey())
								.build();
		JwtClaims receivedClaims = consumer.processToClaims(encryptedJwt);
		System.out.println("SUCESS :: JWT Validation :: " + receivedClaims);
		 //This will throw exception, since we're trying to process without encrypting
		//System.out.println(consumer.processToClaims(encryptedJwt));

		JwtContext jwtCtxt = consumer.process(encryptedJwt);
		
		//Signature verification
		consumer = new JwtConsumerBuilder()
					.setExpectedAudience("Admins")
					.setExpectedIssuer("CA")
					.setVerificationKey(sendersKey.getPublicKey())
					.setRequireSubject()
					.build();
		consumer.processContext(jwtCtxt);
		System.out.println("JWT signature is valid");
		Utils.processClaims(receivedClaims);
	}

}
