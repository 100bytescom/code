package com.one00bytes.jwt;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.keys.resolvers.VerificationKeyResolver;
import org.jose4j.keys.resolvers.X509VerificationKeyResolver;

import sun.security.x509.AlgorithmId;
import sun.security.x509.CertificateAlgorithmId;
import sun.security.x509.CertificateSerialNumber;
import sun.security.x509.CertificateValidity;
import sun.security.x509.CertificateVersion;
import sun.security.x509.CertificateX509Key;
import sun.security.x509.X500Name;
import sun.security.x509.X509CertImpl;
import sun.security.x509.X509CertInfo;


/**
 * Class demonstrates the signing of JWT token using x.509 certificate
 * Similarl to this class, JWT token can be encrypted using x.509 certificate
 * 
 * @author 100bytes.com
 * WEBSITE: http://100bytes.com
 */
public class JWTX509Sign {
	

	public static X509CertImpl certificate = null;
	 
	/** 
	 * Create a self-signed X.509 Certificate
	 * @param dn the X.509 Distinguished Name
	 * @param pair the KeyPair
	 * @param days how many days from now the Certificate is valid for
	 * @param algorithm the signing algorithm, eg "SHA1withRSA"
	 */ 
	public static X509Certificate generateCertificate(String dn, KeyPair pair, int days, String algorithm)
			throws Exception	{
		
		PrivateKey privateKey = pair.getPrivate();

		Date from = new Date();
		Date to = new Date(from.getTime() + days * 1000);
		CertificateValidity interval = new CertificateValidity(from, to);
		BigInteger sn = new BigInteger(64, new SecureRandom());
		X500Name owner = new X500Name(dn);
		
		//Certificate details
		X509CertInfo certInfo = new X509CertInfo();
		certInfo.set(X509CertInfo.ISSUER, owner);
		certInfo.set(X509CertInfo.VALIDITY, interval);
		certInfo.set(X509CertInfo.SERIAL_NUMBER, new CertificateSerialNumber(sn));
		certInfo.set(X509CertInfo.SUBJECT, owner);
		certInfo.set(X509CertInfo.KEY, new CertificateX509Key(pair.getPublic()));
		certInfo.set(X509CertInfo.VERSION, new CertificateVersion(CertificateVersion.V3));
		AlgorithmId algo = new AlgorithmId(AlgorithmId.md5WithRSAEncryption_oid);
		certInfo.set(X509CertInfo.ALGORITHM_ID, new CertificateAlgorithmId(algo));
		 
		certificate = new X509CertImpl(certInfo);
		certificate.sign(privateKey, algorithm);
		
		return certificate;
	}  

	public static void main(String[] args) throws Exception {
		
		
		
		/**************************** SENDER'S END ***********************************/
		 
		JwtClaims claims = new JwtClaims();
		claims.setAudience("Admins");
		claims.setIssuer("CA");
		claims.setSubject("100bytesAdmin");
		claims.setIssuedAtToNow();
		claims.setExpirationTimeMinutesInTheFuture(10); //10 minutes from now
		claims.setGeneratedJwtId();
		claims.setNotBeforeMinutesInThePast(2);
		claims.setClaim("email", "100bytesAdmin@100bytes.com");
		claims.setClaim("Country", "Antartica");
		List<String> hobbies = Arrays.asList("Blogging", "Playing cards", "Games");
		claims.setStringListClaim("hobbies", hobbies);
		System.out.println(claims.toJson());
		
		//SIGNING
		KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
		kpg.initialize(2048);
		KeyPair kp = kpg.generateKeyPair();
		generateCertificate("cn=100bytescom", kp, 10, "SHA1withRSA");
		
		JsonWebSignature jws = new JsonWebSignature();
		jws.setKey(kp.getPrivate());
		jws.setPayload(claims.toJson());
		jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA512);
		jws.setX509CertSha1ThumbprintHeaderValue(certificate);
		String signedJwt = jws.getCompactSerialization();
		System.out.println("Signed ::" + signedJwt);
				
		
		/**************************** RECEIVER'S END ***********************************/ 
		
		//Certificate received from sender
		VerificationKeyResolver certificateResolver = new X509VerificationKeyResolver(certificate);
		JwtConsumer consumer = new JwtConsumerBuilder()
								//Setting certificate to verify the JWT token
								.setVerificationKeyResolver(certificateResolver)
								.setExpectedAudience("Admins")
								.setExpectedIssuer("CA")
								.build();
		JwtClaims receivedClaims = consumer.processToClaims(signedJwt);
		Utils.processClaims(receivedClaims);
	}

}
