package com.one00bytes.jwt.restws.jwk;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jose4j.jwk.JsonWebKeySet;
import org.jose4j.jwk.RsaJsonWebKey;

import sun.misc.BASE64Decoder;


/**
 * This class has to be hosted in a web server and which exposes a REST endpoint where 
 * corresponding public or private key is served. This endpoint can serve more than one keys.
 * Keys are accessed from URL: http://<HOSTNAME>:<PORT_NO>/jwkrestws/resources/jwks/sign
 * 
 * @author 100bytes.com
 * WEBSITE: http://100bytes.com
 */
@Path("/jwks")
public class RestjwkWS {

	@GET
	@Path("/keys")
	@Produces(MediaType.APPLICATION_JSON)
	public String publicKey() throws Exception	{
		
		//Corresponding private key is used in com.one00bytes.jwt.JWTKeyHttpsResolver
		String pubKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiKVMw8mIrTC8024F"
				+ "AXuFufndX/sTNqAPkNkRD9zPWq6nrnWWIVffi5hg8wSMJ6z59EXjWto2CMQcNErRAo"
				+ "pB0jiYjrsNevCtalKRWh/S4HIEBufuDEMxQmvus+AmjjLqkb5CvTQmYjEJjEn62LBs"
				+ "qu4dwOux+d0BZQrcttus0Da0EyJ43cHs79RvtrFBO3/ggVMp1ihQW6bMDcJHXzGRbK"
				+ "KGJWxUPTkUm0wkDrJTwZutoCNj+BMlueyJx8ALE0pFWDd6ZuImiPCAcoE1u/rxlyfq"
				+ "DsoUK7nytjqhknngLWwbESDW+6rFb4nPHMnrCq0bouBOR1MehJlyTuiYRrSjzQIDAQAB";
		BASE64Decoder b64d = new BASE64Decoder();
		byte[] data = b64d.decodeBuffer(pubKey);
	    X509EncodedKeySpec spec = new X509EncodedKeySpec(data);
	    KeyFactory fact = KeyFactory.getInstance("RSA");
	    PublicKey publicKey = fact.generatePublic(spec);
	    
	    //Preparing public key for signature purpose
		RsaJsonWebKey sign_jwk = new RsaJsonWebKey((RSAPublicKey) publicKey);
		sign_jwk.setAlgorithm("RSA");
		sign_jwk.setUse("sig");
		sign_jwk.setKeyId("123452");
		
		JsonWebKeySet jwks = new JsonWebKeySet(sign_jwk);
		return jwks.toJson();
		
		/* SAMPLE CODE TO GENERATE KEYS AND EXPOSED FOR SIGNING AND ENCRYPTION
		  
		KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
		kpg.initialize(2048);
		KeyPair kp = kpg.genKeyPair();
		RsaJsonWebKey enc_jwk = new RsaJsonWebKey((RSAPublicKey) kp.getPublic());
		enc_jwk.setAlgorithm("RSA");
		enc_jwk.setPrivateKey(kp.getPrivate());
		enc_jwk.setUse("enc");
		enc_jwk.setKeyId("123451");
		
		kp = kpg.genKeyPair();
		RsaJsonWebKey sign_jwk = new RsaJsonWebKey((RSAPublicKey) kp.getPublic());
		sign_jwk.setAlgorithm("RSA");
		sign_jwk.setPrivateKey(kp.getPrivate());
		sign_jwk.setUse("sign");
		sign_jwk.setKeyId("123452");
		
		JsonWebKeySet jwks = new JsonWebKeySet(enc_jwk,sign_jwk);*/
	}
}
