package com.one00bytes.jwt.restws.jwk;

import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


/**
 * Supportive class to expose REST endpoint http://<HOSTNAME>:<PORT_NO>/jwkrestws/resources/jwks/keys
 * to serve keys for signature verification and decryption
 * 
 * @author 100bytes.com
 * WEBSITE: http://100bytes.com
 */
@ApplicationPath("resources")
public class ApplicationConfig extends Application {

	public Set<Class<?>> getClasses() {
        return getRestClasses();
    }
    
    private Set<Class<?>> getRestClasses() {
		Set<Class<?>> resources = new java.util.HashSet<Class<?>>();
		
		resources.add(com.one00bytes.jwt.restws.jwk.RestjwkWS.class);
		return resources;    
    }
}