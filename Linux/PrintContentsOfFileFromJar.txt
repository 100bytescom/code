Content of a jar

 Let's assume that we want to examine contents of file com/one00bytes/jar1/jar2/test2.log from jar file one00bytes.jar. 
To execute this command you must know the path of a file which you want to print, the path can be identified by 'jar -tvf'

unzip -p one00bytes.jar com/one00bytes/jar/test1.log

Above command print the contents of com/one00bytes/jar1/jar2/test2.log from one00bytes.jar. 

For next example, I have copied the jar one00bytes.jar  to onebytes1.jar, one00bytes2.jar

 ls
one00bytes.jar  one00bytes1.jar  one00bytes2.jar

 
Content of multiple jars

Now tweak the same command to view the contents of  com/one00bytes/jar1/jar2/test2.log in multiple jars.

find /root/ -name "*.jar" -print -exec unzip -p {} com/one00bytes/jar/test1.log \;

Above command finds all the jars under 'root' folder and print the content of a file from jar. This command will print the following

./one00bytes1.jar
Inside jar - test1.log
./one00bytes.jar
Inside jar - test1.log
./one00bytes2.jar
Inside jar - test1.log

You can redirect the output of the above command to a file, if your screen overflows with output.
